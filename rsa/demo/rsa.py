from rsa.core.rsa import RSAOAEP, BaseRSA, SmallPrivateRSA

if __name__ == "__main__":
    rsa = BaseRSA(1024)
    m = "Hacking Lab 1"
    c = rsa.encrypt(m)
    _m = rsa.decrypt(c)
    print("Original message:", m)
    print("Ciphertext:", c)
    print("Decrypted message:", _m)
    print()

    rsa = SmallPrivateRSA(1024, 128)
    m = "Hacking Lab 2"
    c = rsa.encrypt(m)
    _m = rsa.decrypt(c)
    print("Original message:", m)
    print("Ciphertext:", c)
    print("Decrypted message:", _m)
    print()

    rsa = RSAOAEP(1024)
    m = "Hacking Lab 3"
    c = rsa.encrypt(m)
    _m = rsa.decrypt(c)
    print("Original message:", m)
    print("Ciphertext:", c)
    print("Decrypted message:", _m)