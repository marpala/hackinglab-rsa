from rsa.core.rsa import SmallPrivateRSA
from rsa.attacks.wiener import wiener

if __name__ == "__main__":
    rsa = SmallPrivateRSA(k=1024, bitlength_d=255)
    N, e = rsa.pk
    p, q, d = rsa.sk
    msg = "hacking-lab"
    print("Message:", msg)
    print("Encrypting message...")
    c = rsa.encrypt(msg)
    print("Ciphertext:", c)
    print("Decrypting message...")
    m = rsa.decrypt(c)
    print("Dec. message:", m)


    print("Finding d...")
    result = wiener(N, e)
    result = result[0] if result is not None else None
    print("Guess:", result)

    print("Real: ", d)

