from rsa.core.rsa import RSAOAEP
from rsa.core.oaep_utils import bytesize, OAEP_decode, I2OSP
import rsa.attacks.manger as manger
from math import ceil, log

rsa = RSAOAEP(1024)
m = "hackinglab"


# Attacker's knowledge
N, e = rsa.pk
c = rsa.encrypt(m)
k = bytesize(N)
_k = ceil(log(N, 256))
assert k == _k
assert N < 2**(8*k)
int_c = int.from_bytes(c, 'big')
oracle = manger.Oracle(rsa, k)
B = pow(2, 8*(k-1))
assert 2*B < N

_m = manger.attack(oracle, B, int_c, N, e)

print("RESULTS")
print("Found encoded message:", _m)
print("Decoded message:", OAEP_decode(I2OSP(_m, k-1), b'').decode())
print("Original message:", m)