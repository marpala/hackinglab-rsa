from rsa.core.rsa import RSAOAEP, BaseRSA, SmallPrivateRSA
from rsa.core.oaep_utils import bytesize, OAEP_decode, I2OSP
import rsa.attacks.manger as manger
from rsa.attacks.wiener import wiener
from rsa.core.nt_utils import is_prime
import random
import math
import pytest
import string


###################################################################################################
### Test cryptographic functionality

def test_base_rsa_1():
    rsa = BaseRSA(1024)
    m = "hacking lab"
    c = rsa.encrypt(m)
    _m = rsa.decrypt(c)
    assert m == _m

def test_base_rsa_2():
    rsa = BaseRSA(1024)
    m = str(random.randint(0, 10**9))
    c = rsa.encrypt(m)
    _m = rsa.decrypt(c)
    assert m == _m

def test_small_private_rsa_1():
    rsa = SmallPrivateRSA(1024, 128)
    m = "hacking lab"
    c = rsa.encrypt(m)
    _m = rsa.decrypt(c)
    assert m == _m


def test_small_private_rsa_2():
    rsa = SmallPrivateRSA(1024, 128)
    m = str(random.randint(0, 10**9))
    c = rsa.encrypt(m)
    _m = rsa.decrypt(c)
    assert m == _m


def test_rsa_oaep_1():
    rsa = RSAOAEP(1024)
    m = "hacking lab"
    c = rsa.encrypt(m)
    _m = rsa.decrypt(c)
    assert m == _m


def test_rsa_oaep_2():
    rsa = RSAOAEP(1024)
    m = str(random.randint(0, 10**9))
    c = rsa.encrypt(m)
    _m = rsa.decrypt(c)
    assert m == _m

primes = []
non_primes = []
for x in range(2, 10**5):
    if all(x % y != 0 for y in range(2, int(math.sqrt(x + 1)))):
        primes.append(x)
    else:
        non_primes.append(x)

def test_is_prime_1():
    p = random.choice(primes)
    assert is_prime(p)

def test_is_prime_2():
    n = random.choice(non_primes)
    assert not is_prime(n)

def test_is_prime_3():
    p = 531137992816767098689588206552468627329593117727031923199444138200403559860852242739162502265229285668889329486246501015346579337652707239409519978766587351943831270835393219031728127
    assert is_prime(p)

def test_bitlength_rsa_1():
    k = 1024
    rsa = BaseRSA(k)
    p, q, d = rsa.sk
    assert math.log2(p) - k // 2 <= 0.001
    assert math.log2(q) - k // 2 <= 0.001

def test_bitlength_small_private_rsa_1():
    k = 1024
    bitl_d = 128
    rsa = SmallPrivateRSA(k, bitl_d)
    p, q, d = rsa.sk
    assert q < p
    assert p < 2*q
    assert math.log2(d) - bitl_d <= 0.001


def test_wiener_1():
    rsa = SmallPrivateRSA(k=1024, bitlength_d=255)
    N, e = rsa.pk
    p, q, d = rsa.sk

    result = wiener(N, e)
    
    if result is not None:
        _d, _p, _q = result

        assert d == _d
        assert _p == p or _p == q
        assert _q == p or _q == q


def test_wiener_2():
    rsa = SmallPrivateRSA(k=2048, bitlength_d=511)
    N, e = rsa.pk
    p, q, d = rsa.sk

    result = wiener(N, e)
    
    if result is not None:
        _d, _p, _q = result

        assert d == _d
        assert _p == p or _p == q
        assert _q == p or _q == q


def test_manger_1():
    rsa = RSAOAEP(1024)
    # random message
    m = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))

    N, e = rsa.pk
    c = rsa.encrypt(m)
    k = bytesize(N)
    assert N < 2**(8*k)
    int_c = int.from_bytes(c, 'big')
    oracle = manger.Oracle(rsa, k)
    B = pow(2, 8*(k-1))
    assert 2*B < N

    _m = manger.attack(oracle, B, int_c, N, e)
    _m = OAEP_decode(I2OSP(_m, k-1), b'').decode()
    
    assert m == _m

def test_manger_2():
    rsa = RSAOAEP(2048)
    # random message
    m = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))

    N, e = rsa.pk
    c = rsa.encrypt(m)
    k = bytesize(N)
    assert N < 2**(8*k)
    int_c = int.from_bytes(c, 'big')
    oracle = manger.Oracle(rsa, k)
    B = pow(2, 8*(k-1))
    assert 2*B < N

    _m = manger.attack(oracle, B, int_c, N, e)
    _m = OAEP_decode(I2OSP(_m, k-1), b'').decode()
    
    assert m == _m