import random
from rsa.core.nt_utils import is_prime, mod_inverse
from rsa.core.oaep_utils import *
from math import log2, gcd, log


class BaseRSA:
    """
    Base class for an RSA-based cryptosystem
    """
    def __init__(self, k):
        """
        k: the secret parameter indicating the bitlength of the modulus
        """
        print("----- Schoolbook RSA -----")
        print(f"k = {k}")
        self.setup(k)
    
    def setup(self, k):
        print('Generating primes...')
        self.p = self.gen_prime(k//2)
        self.q = self.gen_prime(k//2)
        while self.q == self.p: # p != q
            self.q = self.gen_prime(k//2)
        self.N = self.p * self.q
        self.phi = (self.p - 1) * (self.q - 1)
        self.gen_exponents()
    
    def gen_exponents(self):
        print('Computing exponents...')
        self.e = 2**16 + 1 # the common choice for regular RSA
        self.d = mod_inverse(self.e, self.phi)

    def gen_prime(self, bitlength):
        """
        Generates a prime number with the given bitlength
        """
        n = 0
        while not is_prime(n):
            n = random.randint(2**(bitlength-1), 2**bitlength - 1)
        return n

    def encrypt(self, m: str) -> int:
        if isinstance(m, str):
            int_m = int.from_bytes(m.encode('utf-8'), byteorder='big')
        else:
            raise ValueError("The message must be of type 'str'")

        if int_m >= self.N:
            raise ValueError("The message must be smaller than the modulus")

        c = pow(int_m, self.e, self.N)

        return c

    def decrypt(self, c: int) -> str:
        if not isinstance(c, int):
            c = int(c)

        if c >= self.N:
            raise ValueError("The ciphertext must be smaller than the modulus")

        int_m = pow(c, self.d, self.N)

        length = ceil(log(int_m, 256))
        m = int.to_bytes(int_m, length=length, byteorder='big').decode('utf-8')

        return m
    
    @property
    def pk(self):
        return self.N, self.e
    
    @property
    def sk(self):
        return self.p, self.q, self.d


class SmallPrivateRSA(BaseRSA):
    """
    Class implementing a weaker version of RSA with a small private exponent
    """
    def __init__(self, k, bitlength_d):
        """
        k: the secret parameter indicating the bitlength of the modulus
        bitlength_d: the bitlength of the secret exponent 'd'
        """
        print("----- Small private exponent RSA -----")
        self.k = k
        self.bitlength_d = bitlength_d
        print(f"k = {k}")
        print(f"bitlength d = {bitlength_d}")
        self.setup()
    
    def setup(self):
        print('Generating primes...')
        self.q = self.gen_prime(self.k//2)
        self.p = self.gen_prime(self.k//2)
        # must have # q < p < 2q
        while self.p == self.q or not (self.p > self.q and self.p < 2*self.q):
            self.p = self.gen_prime(self.k//2)
        self.N = self.p * self.q
        self.phi = (self.p - 1) * (self.q - 1)
        self.gen_exponents()
    
    def gen_exponents(self):
        print('Computing exponents...')
        assert self.bitlength_d > 1 and self.bitlength_d < log2(self.phi), "bitlength_d must not exceed the size of the modulus"

        self.d = random.randint(2**(self.bitlength_d-1), 2**self.bitlength_d - 1)
        while gcd(self.d, self.phi) != 1:
            self.d = random.randint(2**(self.bitlength_d-1), 2**self.bitlength_d - 1)

        assert self.d < self.phi, "d is greater than or equal to the modulus"
        self.e = mod_inverse(self.d, self.phi)

class RSAOAEP(BaseRSA):
    """
    Class implementing the RSA with OAEP padding specificed in PKCS #1 v2.0
    """
    def __init__(self, k):
        """
        k: the secret parameter indicating the bitlength of the modulus
        """
        print("----- RSAES-OAEP -----")
        print(f"k = {k}")
        self.setup(k)

    # https://datatracker.ietf.org/doc/html/rfc2437#section-7.1.1
    def encrypt(self, m: str) -> bytes:
        if isinstance(m, str):
            bytes_m = m.encode()
        else:
            raise ValueError("The message must be of type 'str'")

        k = bytesize(self.N)

        EM = OAEP_encode(bytes_m, b'', k-1)
        int_m = OS2IP(EM)

        c = pow(int_m, self.e, self.N)
        c = I2OSP(c, k)
        return c

    # https://datatracker.ietf.org/doc/html/rfc2437#section-7.1.2
    def decrypt(self, c: bytes) -> str:
        k = bytesize(self.N)

        if len(c) != k:
            raise ValueError("decryption error")
        
        int_c = OS2IP(c)

        if int_c >= self.N:
            raise ValueError("decryption error")

        _m = pow(int_c, self.d, self.N)

        try:
            EM = I2OSP(_m, k-1)
        except ValueError as e:
            # In order to avoid Manger's attack, we need to make sure (among others)
            # this exception and the following one yield the same message.
            # Manger assumes a weak implementation where this is not the case.
            # Here we weaken our implementation by distinguishing the two errors.
            # Instead of raising a 'decryption error', we raise whatever I2OSP raises
            # (integer too large).
            raise e

        try:
            bytes_m = OAEP_decode(EM, b'')
        except ValueError:
            raise ValueError("decryption error")

        m = bytes_m.decode()
        return m