import random
import sys
sys.setrecursionlimit(10000)

# Module containing implementations of various number theoretic algorithms

def miller_rabin(n: int, rounds=40) -> bool:
    """
    Implementation of the Miller-Rabin primality test.
    Probabilistically check whether 'n' is prime over <= 40 rounds (default).
    """
    if n == 2:
        return True

    if n % 2 == 0:
        return False

    r, s = 0, n - 1
    while s % 2 == 0:
        r += 1
        s //= 2

    for _ in range(rounds):
        a = random.randrange(2, n - 1)
        x = pow(a, s, n)
        if x == 1 or x == n - 1:
            continue
        for _ in range(r - 1):
            x = pow(x, 2, n)
            if x == n - 1:
                break
        else:
            return False
    return True

# Miller-Rabin test
# https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test
def is_prime(n):
    return miller_rabin(n)

def mod_inverse(a, m):
    return pow(a, -1, m)

def cont_frac(n, d):
    """
    Recursive algorithm to find the continued fractions expansion of n / d
    """
    if d == 0:
        return []
    q = n // d            
    r = n % d         
    return [q] + cont_frac(d, r)


# https://en.wikipedia.org/wiki/Continued_fraction#Infinite_continued_fractions_and_convergents
def convergents(cf):
    """
    Computes the convergents from a continued fractions expansion 'cf'
    """
    conv = []
    if len(cf) >= 1:
        conv.append(
            (cf[0], 1)
        )
    if len(cf) >= 2:
        conv.append(
            (cf[0] * cf[1] + 1,  cf[1])
        )
    if len(cf) >= 3:
        conv.append(
            ((cf[2] * (cf[0]*cf[1] + 1) + cf[0]), (cf[1] * cf[2] + 1))
        )
    if len(cf) >= 4:
        conv.append(
            ((cf[3]*(cf[2]*(cf[1]*cf[0] + 1)) + (cf[1]*cf[0] + 1)), (cf[3]*(cf[2] * cf[1] + 1) + cf[1]))
        )
    
    if len(cf) > 4:
        for i in range(4, len(cf)):
            conv.append(
                (cf[i]*conv[-1][0] + conv[-2][0], cf[i]*conv[-1][1] + conv[-2][1])
            )

    return conv


# https://en.wikipedia.org/wiki/Integer_square_root
def isqrt(n: int) -> int:
    """
    Computes the integer square root of 'n'
    """

    assert n >= 0, "sqrt works for only non-negative inputs"
    if n < 2:
        return n

    shift = 2
    while (n >> shift) != 0:
        shift += 2

    result = 0
    while shift >= 0:
        result = result << 1
        large_cand = result + 1
        if large_cand * large_cand <= n >> shift:
            result = large_cand
        shift -= 2

    return result


def perf_square(n):
    """
    Check whether 'n' is a perfect square or not
    """
    result = isqrt(n)

    if result*result == n:
        return result
    else:
        return None
