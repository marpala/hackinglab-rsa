from math import ceil
import hashlib
from secrets import token_bytes


def bytesize(n):
    """
    Returns the byte size of the given positive integer 'n'
    """
    if n < 0:
        raise ValueError("'n' must be a positive integer")
    n_bits = 0
    while n >> n_bits:
        n_bits += 1
    return int(ceil(n_bits/8))


def xor_bytes(first, second):
    """
    XOR two byte streams
    """
    return bytes(a ^ b for (a, b) in zip(first, second))


# https://datatracker.ietf.org/doc/html/rfc2437#section-4.1
def I2OSP(x, xLen, endian='big') -> bytes:
    """
    Converts a nonnegative integer to a byte string of a specified length
    """
    if x >= 256**xLen: # x >= B
        raise ValueError("integer too large")

    byte_arr = []

    while x:
        byte_arr.append(x % 256)
        x = x // 256

    for _ in range(xLen - len(byte_arr)):
        byte_arr.append(0)
    
    if endian == 'big':
        return bytes(byte_arr[::-1])
    
    return bytes(byte_arr)

# https://datatracker.ietf.org/doc/html/rfc2437#section-4.2
def OS2IP(byte_arr, endian='big') -> int:
    """
    Converts a byte string to a nonnegative integer
    """
    xLen = len(byte_arr)

    if endian == 'big':
        byte_arr = byte_arr[::-1]

    x = 0
    for i in range(xLen):
        x += byte_arr[i] * 256**i

    return x


# https://datatracker.ietf.org/doc/html/rfc2437#section-10.2.1
def MGF1(mgfSeed, maskLen):
    """
    Takes an octet string of variable length and a desired output length as input,
    and outputs an octet string of the desired length. Based on SHA1.
    Input:
      mgfSeed: seed from which mask is generated, an octet string
      maskLen: intended length in octets of the mask, at most 2^32 hLen
    Output:
      mask: mask, an octet string of length maskLen
    """

    hLen = hashlib.sha1().digest_size

    if maskLen > 2**32 * hLen:
        raise ValueError("mask too long")

    T = b""
    for count in range(0, ceil(maskLen / hLen)):
        count = I2OSP(count, 4)
        T += hashlib.sha1(mgfSeed + count).digest()
    return T[:maskLen] # big endian


# https://datatracker.ietf.org/doc/html/rfc2437#section-9.1.1.1
def OAEP_encode(bytes_m, p, emLen) -> bytes:
    """
    Input:
        M: message to be encoded, an octet string of length at most emLen-1-2hLen
        P: encoding parameters, an octet string
        emLen: intended length in octets of the encoded message, at least 2hLen+1
    Output:
        EM: encoded message, an octet string of length emLen
    """
    mLen = len(bytes_m)
    hLen = hashlib.sha1().digest_size

    if mLen > emLen - 2*hLen - 1:
        raise ValueError("message too long")

    pHash = hashlib.sha1(p).digest()
    PS = b'\x00' * (emLen - mLen - 2*hLen - 1)
    DB = pHash + PS + b'\x01' + bytes_m
    seed = token_bytes(hLen)
    dbMask = MGF1(seed, emLen - hLen)
    maskedDB = xor_bytes(DB, dbMask)
    seedMask = MGF1(maskedDB, hLen)
    maskedSeed = xor_bytes(seed, seedMask)
    EM = maskedSeed + maskedDB # OAEP encoded message
    return EM

# https://datatracker.ietf.org/doc/html/rfc2437#section-9.1.1.2
def OAEP_decode(EM, p) -> bytes:
    """
    Decodes an OAEP-padded message EM into its original byte representation
    """
    hLen = hashlib.sha1().digest_size

    if len(EM) < 2*hLen + 1:
        raise ValueError("decoding error")


    maskedSeed = EM[:hLen]
    maskedDB = EM[hLen:len(EM) + hLen]
    seedMask = MGF1(maskedDB, hLen)
    seed = xor_bytes(maskedSeed, seedMask)
    dbMask = MGF1(seed, len(EM) - hLen)
    DB = xor_bytes(maskedDB, dbMask)
    pHash = hashlib.sha1(p).digest()
    _pHash = DB[:hLen]
    sep = DB[hLen:].find(b'\x01')

    if sep < 0: # not found
        raise ValueError("decryption error")

    if pHash != _pHash:
        raise ValueError("decryption error")

    bytes_m = DB[hLen + sep + 1:]

    return bytes_m