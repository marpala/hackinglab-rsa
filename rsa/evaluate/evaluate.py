from time import time_ns, strftime
from rsa.core.rsa import RSAOAEP, SmallPrivateRSA
from rsa.attacks.wiener import wiener
import pandas as pd
import os
from rsa.core.rsa import RSAOAEP
from rsa.core.oaep_utils import bytesize, OAEP_decode, I2OSP
import rsa.attacks.manger as manger
from math import ceil, log

# This code runs the Wiener and Manger attacks 10 times for each key size in ks
# The average running time in seconds for each key size is then computed
# For the Manger attack the average number of oracle calls is also computed

if __name__ == "__main__":
    ks = [512, 1024, 2048, 4096, 8192]

    # ---- Wiener
    ds = [n//4-1 for n in ks]
    results = []
    run_id = 'run_wiener_' + strftime("%Y%m%d%H%M%S")
    for i in range(len(ks)):
        times = []
        runs = 0
        while runs != 10:
            rsa = SmallPrivateRSA(k=ks[i], bitlength_d=ds[i])
            N, e = rsa.pk
            p, q, d = rsa.sk
            start = time_ns()
            result = wiener(N, e)
            elapsed = time_ns() - start
            if result is not None:
                times.append(elapsed)
                print(times)
                d_guess = result[0]
                assert d == d_guess
                runs += 1
        results.append((ks[i], ds[i], sum(times)/len(times)/1e9))
        df = pd.DataFrame(data=results, columns=['k', 'd', 'time (s)'])
        df.to_csv(os.getcwd() + f'/rsa/evaluate/results/{run_id}.csv', index=False)


    # # ---- Manger
    results = []
    run_id = 'run_manger_' + strftime("%Y%m%d%H%M%S")
    for i in range(len(ks)):
        times = []
        oracle_calls = []
        runs = 0
        while runs != 10:
            rsa = RSAOAEP(ks[i])
            m = "hackinglab"
            # Attacker's knowledge
            N, e = rsa.pk
            c = rsa.encrypt(m)
            k = bytesize(N)
            _k = ceil(log(N, 256))
            assert k == _k
            assert N < 2**(8*k)
            int_c = int.from_bytes(c, 'big')
            oracle = manger.Oracle(rsa, k)
            B = pow(2, 8*(k-1))
            assert 2*B < N
            
            start = time_ns()
            _m = manger.attack(oracle, B, int_c, N, e)
            _m = OAEP_decode(I2OSP(_m, k-1), b'').decode()
            elapsed = time_ns() - start
            assert _m == m
            times.append(elapsed)
            oracle_calls.append(oracle.n_calls)
            runs += 1
        results.append((ks[i], sum(times)/len(times)/1e9, sum(oracle_calls)/len(oracle_calls)))
        df = pd.DataFrame(data=results, columns=['k', 'time (s)', 'oracle calls'])
        df.to_csv(os.getcwd() + f'/rsa/evaluate/results/{run_id}.csv', index=False)
