class Oracle:
    """
    This oracle is implemented by using error information output by the RSAES-OAEP decryption algorithm.
    """
    def __init__(self, rsa, k):
        self.__rsa = rsa
        self.__k = k
        self.n_calls = 0
    
    def query(self, x: int):
        """
        If OAEP(Dec(x)) >= B : True
        If OAEP(Dec(x)) < B : False
        """
        self.n_calls += 1

        try:
            x = x.to_bytes(self.__k, 'big')
            self.__rsa.decrypt(x)
        except ValueError as e:
            if str(e) != 'decryption error':
                return True

        return False


def attack(oracle, B, c, N, e):
    """
    Implementation of Manger's attack. Each step is implemented exactly
    as presented in the original paper.
    """
    # ceiling division, works with large ints to avoid precision errors
    ceildiv = lambda x, y: x // y + (x % y > 0)

    print("-------- STEP 1 --------")
    f1 = 2
    while not oracle.query((pow(f1, e, N) * c) % N):
        f1 *= 2
    
    print("Nr. oracle calls:", oracle.n_calls)

    print("-------- STEP 2 --------")
    f2 = (N + B)//B * f1//2
    while oracle.query((pow(f2, e, N) * c) % N):
        f2 += f1 // 2

    print("Nr. oracle calls:", oracle.n_calls)

    print("-------- STEP 3 --------")
    m_min = ceildiv(N, f2)
    m_max = (N + B) // f2

    while m_min < m_max:
        f_tmp = 2*B // (m_max - m_min)
        i = f_tmp*m_min // N
        f3 = ceildiv(i*N, m_min)
        
        if not oracle.query((pow(f3, e, N) * c) % N):
            m_max = (i*N + B) // f3
        else:
            m_min = ceildiv(i*N + B, f3)

    print("Nr. oracle calls:", oracle.n_calls)

    return m_min