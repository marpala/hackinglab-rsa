from rsa.core.nt_utils import cont_frac, convergents, perf_square
import random
from math import gcd

def factor_from_d(N, e, d):
    """
    Recovers the prime factors from a modulus if the private exponent is known
    """
    k = e*d - 1
    c = 0

    while k % (2**c) == 0:
        c += 1

    while True:
        x = random.randrange(1, N)

        for t in range(1, c+1):
            y = pow(x, k // (2**t), N)
            p = gcd(y - 1, N)

            if p != 1 and p != N and N % p == 0:
                q = N // p
                return p, q

def wiener(N, e):
    """
    Perform Wiener's attack on an instance of RSA with a small private exponent
    """
    cf = cont_frac(e, N)
    convs = convergents(cf)

    for k, d in convs:

        if k > 0 and (e*d -1) % k == 0:
            phi_guess = (e*d - 1) // k
            b = N - phi_guess + 1
            discriminant = b*b - 4*N

            if discriminant >= 0:
                # discriminant must be a perfect square since everything is an integer
                root = perf_square(discriminant)

                if root is not None and (b+root) % 2 == 0:
                    p, q = factor_from_d(N, e, d)
                    return d, p, q

    print("The attack failed")
    return None
