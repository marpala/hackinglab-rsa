# hacking-lab
 
This code was tested on Python 3.10.0 running on macOS.

Make sure the dependencies are installed by running:

```
pip install -r requirements.txt
```

To run one of the demo, go to the `hackinglab-rsa/` directory (the root of this repo) and run:

```
python -m rsa.demo.{rsa|manger|wiener}
```

The tests can be run by going to the `tests/` directory and running:

```
pytest
```